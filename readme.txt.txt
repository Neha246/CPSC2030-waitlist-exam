Please recreate the site found in the pdf file.
include all html, css, and javascript files


For those people on the waitlist and want to get into the course!
*Exam instructions
1) create a gitlab account
2) setup citrxi(if you are using citrix) to ssh keys, since cloning from https does not work
3) fork the exam repo to your account
4) Add the instructor as a developer to the repo, so he can see your work
5) clone your repo to the local computer
6) recreate the site found in pdf
7) commit your work to your repo
8) e-mail the instructor the link to your repo

*you cannot ask the instructor for help
*you can use the internet